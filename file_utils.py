import os, shutil, glob, fnmatch,sys
import pandas as pd
from tqdm import tqdm

# get the size of file

args = sys.argv[1:]
print('The usage----------\n')
print('python3 file_utils.py input_dir(path/to/wsi) output_dir revised_tsv_file.tsv(path/to/result_revised.csv) force(optional if you would like to re-run and dir already exists)')
spath = args[0]
dpath = args[1]
tsv_file_path = args[2]

df = pd.read_csv(tsv_file_path,sep='\t',skiprows=3)
df = list(df.iloc[2:,0])

fpath = sorted(glob.glob(spath+'*.svs'))

if not os.path.exists(dpath): os.mkdir(dpath)
elif args[3] == '--force': 
    shutil.rmtree(dpath)
    os.mkdir(dpath)

def find(pattern, path):
    result = []
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))
    return result
    
# totalDir = totalFiles = 0
# for base, dirs, files in os.walk(spath):
#     print('Searching in : ',base)
#     for directories in dirs:
#         totalDir += 1
#     for Files in files:
#         totalFiles += 1

for each_image in tqdm(range(len(df))):	
	if find(df[each_image], spath):		
		shutil.copy(spath+df[each_image], dpath)
		# print(f'[INFO]: Image id: {each_image} -- size: {size} GB')


